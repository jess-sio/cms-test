from setuptools import setup, find_packages

import rooms


setup(
    name='rooms',
    version=0.1,
    description='',
    author='Author Here',
    author_email='author.here@gmail.com',
    include_package_data=True,
    packages=find_packages(),
    classifiers=[
    ],
    zip_safe=False,
)
