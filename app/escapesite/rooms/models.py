from django.db import models


ROOM_THEMES = (
    ("", "Unknown"),
    ("ancient-worlds", "Ancient Worlds"),
    ("apocalypse", "Apocalypse"),
    ("christmas", "Christmas"),
    ("desert-island", "Desert Island"),
    ("detective", "Detective"),
    ("halloween", "Halloween"),
    ("heist", "Heist"),
    ("historic", "Historic"),
    ("local-tourism", "Local tourism"),
    ("magic", "Magic (Witches & Wizards)"),
    ("pirates", "Pirates"),
    ("prison-break", "Prison Break"),
    ("scifi", "Science Fiction"),
    ("space", "Space Sci-Fi"),
    ("treasure-hunt", "Treasure Hunt"),
    ("zombie", "Zombies"),
    ("other", "Other"),
)

ROOM_TYPES = (
    ("", "Unknown"),
    ("escape-room", "Escape Room"),
    ("open-air", "Open Air"),
    ("vr", "Virtual Reality"),
    ("tabletop", "Tabletop Game"),
    ("other", "Other"),
)


class Game(models.Model):
    """
    A specific game: a story with a theme and a goal.
    """

    name = models.CharField(max_length=255, help_text="The Name of the Game")
    description = models.TextField(
        blank=True, help_text="The game's full description (markdown)"
    )
    difficulty = models.PositiveSmallIntegerField(
        default=0, blank=True, help_text="Difficulty (1-100 scale)"
    )
    success_rate = models.PositiveSmallIntegerField(
        default=0, blank=True, help_text="Estimated success rate (percentage)"
    )
    scariness = models.PositiveSmallIntegerField(
        default=0, blank=True, help_text="Fear level: How scary the room is (1-100 scale)"
    )
    min_age = models.PositiveSmallIntegerField(
        default=0, blank=True, help_text="Minimum age of players"
    )
    min_age_supervised = models.PositiveSmallIntegerField(
        default=0, blank=True, help_text="Minimum age of players when supervised by an adult"
    )
    max_age = models.PositiveSmallIntegerField(
        default=0,
        blank=True,
        help_text="Maximum age of players (only set for children's games)",
    )
    min_players = models.PositiveSmallIntegerField(
        help_text="Minimum amount of supported players"
    )
    max_players = models.PositiveSmallIntegerField(
        help_text="Maximum amount of supported players"
    )
    recommended_min_players = models.PositiveSmallIntegerField(
        null=True, blank=True, help_text="Recommended minimum amount of players"
    )
    recommended_max_players = models.PositiveSmallIntegerField(
        null=True, blank=True, help_text="Recommended maximum amount of players"
    )
    theme = models.CharField(
        max_length=64, choices=ROOM_THEMES, help_text="The main theme of the game"
    )
    type = models.CharField(
        max_length=64, choices=ROOM_TYPES, help_text="Type of the game"
    )
