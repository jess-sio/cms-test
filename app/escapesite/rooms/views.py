from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader

from .models import Game


def index(request):
    games_list = Game.objects.order_by('name')
    template = loader.get_template('rooms/index.html')
    context = {
        'games_list': games_list
    }
    return HttpResponse(template.render(context, request))


def detail(request, game_id):
    return HttpResponse(f"Escape room {game_id}")
