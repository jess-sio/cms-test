```sh
cd app
poetry install
cd escapesite
poetry run python manage.py makemigrations rooms
poetry run python manage.py migrate
# for escapesite, I was following and left off midway through https://docs.djangoproject.com/en/3.0/intro/tutorial03/

cd ../..

cd cms
poetry install
cd escapecms
poetry run python manage.py migrate rooms
# for escapecms, I was following http://docs.django-cms.org/en/latest/introduction/03-integrating_applications.html
# I skipped three sections after this to get to the wizard stuff
# "3.1.2. Improve the templates for Polls" breaks for me

poetry run python manage.py runserver
```