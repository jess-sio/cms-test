from cms.wizards.wizard_base import Wizard
from cms.wizards.wizard_pool import wizard_pool

from rooms_cms_integration.forms import GameWizardForm


class PollWizard(Wizard):
    def get_success_url(self, obj, **kwargs):
        """
        This should return the URL of the created object, «obj».
        """
        return f"http://127.0.0.1:8000/en/rooms/{obj.id}/"


game_wizard = PollWizard(
    title="Game",
    weight=200,  # determines the ordering of wizards in the Create dialog
    form=GameWizardForm,
    description="Add a new Game",
)

wizard_pool.register(game_wizard)
