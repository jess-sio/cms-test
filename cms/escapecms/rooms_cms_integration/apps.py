from django.apps import AppConfig


class RoomsCmsIntegrationConfig(AppConfig):
    name = 'rooms_cms_integration'
