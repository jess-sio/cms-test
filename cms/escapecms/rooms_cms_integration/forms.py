from django import forms

from rooms.models import Game


class GameWizardForm(forms.ModelForm):
    class Meta:
        model = Game
        fields = ['name', 'description', 'min_players', 'max_players']
